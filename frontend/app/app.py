from flask import Flask, render_template, request, redirect
import requests
import csv
import boto3
import os
from dotenv import load_dotenv
load_dotenv()
aws_access_key_id = os.getenv('aws_access_key_id')
aws_secret_access_key = os.getenv('aws_secret_access_key')


session = boto3.session.Session()

s3_client = session.client(
    service_name = 's3',
    endpoint_url = 'http://82.146.57.75:9000',
    aws_access_key_id = aws_access_key_id,
    aws_secret_access_key = aws_secret_access_key
)
try:
    s3_client.create_bucket(Bucket='forcsv')
except: print("Has already") 


def createcsv(id):
    dt = str(id)
    tt = str(dt + '.csv')
    file_name = tt
    s3_list = []
    try: 
        url = 'http://backend:5000/gameid/'+str(id)
        r = requests.get(url).json()
        i = 0
        Date = str(r['Date'])
        Home_Command_Name = str(r['Command']['Home']['Name'])
        Away_Command_Name = str(r['Command']['Away']['Name'])
        Home_Command_Goals = str(r['Command']['Home']['Goals'])
        Away_Command_Goals = str(r['Command']['Away']['Goals'])
        detail_game_home = []
        detail_game_away = []
        while i <  18:
            fullname_player_home = r['Command']['Home']['Data'][i]['fullname_player']
            toi_home = r['Command']['Home']['Data'][i]['toi']
            detail_home = [
                    'fullname_player', fullname_player_home,
                    'toi', toi_home,
            ]
            detail_game_home.append(detail_home)
            fullname_player_away = r['Command']['Away']['Data'][i]['fullname_player']
            toi_away = r['Command']['Away']['Data'][i]['toi']
            detail_away = [
                    'fullname_player', fullname_player_away,
                    'toi', toi_away,
            ]
            detail_game_away.append(detail_away)
            i += 1
        url2 = 'http://backend:5000/top3/'+str(id)
        r2 = requests.get(url2).json()
        i = 0

        top3_player = []
        while i <  3:
            fullname_player = r2['top3_player']['Data'][i]['fullname_player']
            toi = r2['top3_player']['Data'][i]['toi']
            detail = [
                    'fullname_player', fullname_player,
                    'toi', toi,
            ]
            top3_player.append(detail)
            i += 1
        for key in s3_client.list_objects(Bucket='forcsv')['Contents']:
            list_s3 = key['Key']
            s3_list.append(list_s3)
        if file_name not in s3_list: 
            with open(file_name, mode='w+') as employee_file:
                employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                employee_writer.writerow([top3_player])
                employee_writer.writerow([Home_Command_Name, detail_game_home])
                employee_writer.writerow([Away_Command_Name, detail_game_away])
            s3_client.upload_file(file_name, 'forcsv', file_name)
            os.remove(file_name)
    except:
        return "Error Get Date"


app = Flask(__name__)

@app.route("/", methods = ['GET'])
def index():
    try:   
        url = 'http://backend:5000/games'
        r = requests.get(url).json()
        return render_template("index.html", confs = r)
    except:
        return "Error Get Date"


@app.route('/files/<int:id>')
def files(id):
    createcsv(id)
    idd = str(id)
    return redirect("http://82.146.57.75:9000/forcsv/"+idd+".csv")
    # return send_from_directory('database_reports', idd)

@app.route('/gameid/<int:id>')
def gameid(id):
    try: 
        url = 'http://backend:5000/gameid/'+str(id)
        r = requests.get(url).json()
        i = 0
        Date = str(r['Date'])
        Home_Command_Name = str(r['Command']['Home']['Name'])
        Away_Command_Name = str(r['Command']['Away']['Name'])
        Home_Command_Goals = str(r['Command']['Home']['Goals'])
        Away_Command_Goals = str(r['Command']['Away']['Goals'])
        detail_game_home = []
        detail_game_away = []
        while i <  18:
            fullname_player_home = r['Command']['Home']['Data'][i]['fullname_player']
            toi_home = r['Command']['Home']['Data'][i]['toi']
            detail_home = [
                    'fullname_player', fullname_player_home,
                    'toi', toi_home,
            ]
            detail_game_home.append(detail_home)
            fullname_player_away = r['Command']['Away']['Data'][i]['fullname_player']
            toi_away = r['Command']['Away']['Data'][i]['toi']
            detail_away = [
                    'fullname_player', fullname_player_away,
                    'toi', toi_away,
            ]
            detail_game_away.append(detail_away)
            i += 1
        url2 = 'http://backend:5000/top3/'+str(id)
        r2 = requests.get(url2).json()
        i = 0

        top3_player = []
        while i <  3:
            fullname_player = r2['top3_player']['Data'][i]['fullname_player']
            toi = r2['top3_player']['Data'][i]['toi']
            detail = [
                    'fullname_player', fullname_player,
                    'toi', toi,
            ]
            top3_player.append(detail)
            i += 1

        return render_template("game.html",id=id,Date = Date,top3_player = top3_player, detail_game_away = detail_game_away, detail_game_home = detail_game_home, Home_Command_Name = Home_Command_Name,Away_Command_Name = Away_Command_Name, Home_Command_Goals= Home_Command_Goals, Away_Command_Goals =Away_Command_Goals )
    except:
        return "Error Get Date"

if __name__ == "__main__":
    app.run(debug = True,host='0.0.0.0', port=80)


