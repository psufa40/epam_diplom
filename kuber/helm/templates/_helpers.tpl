{{/*
My Release Namespace 
*/}}
{{- define "epam.namespace" -}}
  {{- if ne .Release.Namespace "default" -}}
    {{- .Release.Namespace -}}
  {{- else -}}
    {{ .Values.Namespace }}
  {{- end -}}
{{- end -}}

{{/*
My labels
*/}}
{{- define "epam.labels" -}}
{{- $r := dir .Template.Name -}}
{{- $d := (split "/" .Template.Name)._2 -}}
app: {{ $d }}
{{- end }}


{{/*
Expand the name of the chart.
{{ "APP" }}
{{- $r := dir .Template.Name -}}
{{- regexSplit "epam/templates/" $r -1 -}}
*/}}
{{- define "epam.name" -}}
{{- $d := (split "/" .Template.Name)._2 -}}
{{- $d -}}

{{- end }}




{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "epam.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "epam.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "epam.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "epam.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

