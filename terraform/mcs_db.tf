 terraform {
    required_providers {
      yandex = {
        source = "yandex-cloud/yandex"
      }
    }
  }
  provider "yandex" {
    token     = var.key["token"]
    cloud_id  = var.key["cloud_id"]
    folder_id = var.key["folder_id"]
    zone      = "ru-central1-a"
  }
  
resource "yandex_vpc_network" "epam" {
  name = "Epam_Network"
}
 resource "yandex_vpc_subnet" "epam" {
    zone           = "ru-central1-a"
    network_id     = "${yandex_vpc_network.epam.id}"
    v4_cidr_blocks = ["10.1.0.0/24"]
  }

resource "yandex_vpc_security_group" "group1" {
  name        = "mysql"
  description = "Mysql Public Access"
  network_id  = "${yandex_vpc_network.epam.id}"

  labels = {
    my-label = "Mysql"
  }

  ingress {
    protocol       = "TCP"
    description    = "Mysql"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 3306
  }
}
resource "yandex_mdb_mysql_cluster" "epam" {
  name        = "Epam"
  environment = "PRESTABLE"
  network_id  = yandex_vpc_network.epam.id
  version     = "8.0"

  resources {
    resource_preset_id = "b2.nano"
    disk_type_id       = "network-ssd"
    disk_size          = 16
  }

  mysql_config = {
    sql_mode                      = "ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION"
    max_connections               = 100
    default_authentication_plugin = "MYSQL_NATIVE_PASSWORD"
    innodb_print_all_deadlocks    = true

  }

  access {
    web_sql = true
  }

  database {
    name = "backend"
  }

  user {
    name     = var.key["username_db"]
    password = var.key["password_db"]
    permission {
      database_name = "backend"
      roles         = ["ALL"]
    }
  }

  host {
    zone      = "ru-central1-a"
    subnet_id = yandex_vpc_subnet.epam.id
    assign_public_ip = "true"
  }
}
output "fqdn" {
  value       = yandex_mdb_mysql_cluster.epam.host[0].fqdn
  description = "The private IP address of the main server instance."
}