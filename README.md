This Is My Diploma in Epam School

Full Task

Variant 10. To attract hockey fans to marketing campaign you will need to find all games happened in St. Louis during previous month (e.g. if now is March 05, 2021 last month means Feb 2021) and print a list of it with score and top 3 player by time on ice. Use https://gitlab.com/dword4/nhlapi API and store all the collected data in a local database.


Let me remind you that it has status of a Diploma task which demonstrate success of your finishing the course. It is highly recommended to perform it during the education (you can find recommended modules in the common part). If you defend your diploma before finishing the modules, it will allow you to be hired during the education.
