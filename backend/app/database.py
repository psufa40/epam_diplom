import requests
import mysql.connector as mariadb
from datetime import date, timedelta, datetime
import calendar
import os
import os
from dotenv import load_dotenv
load_dotenv()
db_user = os.getenv('db_user')
db_password = os.getenv('db_password')
db_host = os.getenv('db_host')


def insert_data():
    connection = mariadb.connect(
        host=db_host,
        ssl_ca='root.crt',
        port= 3306,
        user=db_user,
        password=db_password,
        database='backend')
    cursor = connection.cursor()

    cursor.execute("CREATE TABLE IF NOT EXISTS game_home(id INT NOT NULL auto_increment,date varchar(30),gameid INT not null,fullname_player varchar(255) NOT NULL,toi varchar(20) NOT NULL,primary key(id))")
    cursor.execute("CREATE TABLE IF NOT EXISTS game_away(id INT NOT NULL auto_increment,date varchar(30),gameid INT not null,fullname_player varchar(255) NOT NULL,toi varchar(20) NOT NULL,primary key(id))")
    cursor.execute("CREATE TABLE IF NOT EXISTS games(id INT NOT NULL auto_increment,date varchar(30),gameid INT not null UNIQUE,team_name_home varchar(100) NOT NULL,team_name_away varchar(100) NOT NULL,goals_home INT not null,goals_away INT not null,primary key(id))")

    delta = timedelta(days=1)
    now = datetime.now()
    start_date = date(now.year, (now.month-1), 1)
    end_date = date(now.year,(now.month-1), calendar.monthrange(now.year, (now.month-1))[1])

    while start_date <= end_date:
        ddate = str(start_date)
        url = 'https://statsapi.web.nhl.com/api/v1/schedule?expand=schedule.linescore&date='+ddate
        r = requests.get(url).json()
        i = 0
        while i < (r['totalGames']):
            if (r['dates'][0]['games'][i]['venue']['name']) == "Enterprise Center":
                gameId = str(r['dates'][0]['games'][0]['gamePk'])
                urlboxscore = 'https://statsapi.web.nhl.com/api/v1/game/'+gameId+'/boxscore'
                r2 = requests.get(urlboxscore).json()
                home_skaters = r2['teams']['home']['skaters']
                home_scrathes = r2['teams']['home']['scratches']
                home_players=list(set(home_skaters) ^ set(home_scrathes))
                home_players_details = []
                for play in home_players:
                    id_players = r2['teams']['home']['players']['ID'+str(play)]
                    fullName = id_players['person']['fullName']
                    timeOnIce = id_players['stats']['skaterStats']['timeOnIce']
                    team_name_home = r2['teams']['home']['team']['name']
                    goals_home = r2['teams']['home']['teamStats']['teamSkaterStats']['goals']
                    detail = [
                        'FullName:', fullName,
                        'timeOnIce:', timeOnIce,
                    ]
                    home_players_details.append(detail)
                away_skaters = r2['teams']['away']['skaters']
                away_scrathes = r2['teams']['away']['scratches']
                away_players=list(set(away_skaters) ^ set(away_scrathes))
                away_players_details = []
                for play in away_players:
                    id_players = r2['teams']['away']['players']['ID'+str(play)]
                    fullName = id_players['person']['fullName']
                    timeOnIce = id_players['stats']['skaterStats']['timeOnIce']
                    team_name_away = r2['teams']['away']['team']['name']
                    goals_away = r2['teams']['away']['teamStats']['teamSkaterStats']['goals'] 
                    detail = [
                        'FullName:', fullName,
                        'timeOnIce:', timeOnIce,
                    ]
                    away_players_details.append(detail)
    # Function for tables game_home 
                for ii in home_players_details:
                    sql = "SELECT * FROM game_home WHERE fullname_player = %s and gameid = %s"
                    cursor.execute(sql, (ii[1], gameId))
                    confs = cursor.fetchall()
        #Function If Else needed for unique insert date in databases
                    count = len(confs)
                    if count == 0:
                        sql = "INSERT INTO game_home (date,gameid,fullname_player,toi) VALUES (%s, %s,%s, %s)"
                        val = (ddate,gameId,ii[1],ii[3])
                        cursor.execute(sql,val)
                        connection.commit()
                    else:
                        sql = "SELECT * FROM game_home WHERE fullname_player = %s and gameid = %s"
                        cursor.execute(sql, (ii[1], gameId))
                        confs = cursor.fetchall()
                        for ff in confs:
                            if str(ff[2]) != gameId and ff[5] != ii[1]:
                                sql = "INSERT INTO game_home (date,gameid,fullname_player,toi) VALUES (%s, %s,%s, %s)"
                                val = (ddate,gameId,ii[1],ii[3])
                                cursor.execute(sql,val)
                                connection.commit()
    # Function for tables game_away  
                for ii in away_players_details:
                    sql = "SELECT * FROM game_away WHERE fullname_player = %s and gameid = %s"
                    cursor.execute(sql, (ii[1], gameId))
                    confs = cursor.fetchall()
    #     # #Function If Else needed for unique insert date in databases
                    count = len(confs)
                    if count == 0:
                        sql = "INSERT INTO game_away (date,gameid,fullname_player,toi) VALUES (%s, %s,%s, %s)"
                        val = (ddate,gameId,ii[1],ii[3])
                        cursor.execute(sql,val)
                        connection.commit()
                    else:
                        sql = "SELECT * FROM game_away WHERE fullname_player = %s and gameid = %s"
                        cursor.execute(sql, (ii[1], gameId))
                        confs = cursor.fetchall()
                        for ff in confs:
                            if str(ff[2]) != gameId and ff[5] != ii[1]:
                                sql = "INSERT INTO game_away (date,gameid,fullname_player,toi) VALUES (%s, %s,%s, %s)"
                                val = (ddate,gameId,ii[1],ii[3])
                                cursor.execute(sql,val)
                                connection.commit()
    # Function for tables games     
                sql = "SELECT * FROM games WHERE gameid = %s"
                cursor.execute(sql, (gameId,))
                confs = cursor.fetchall()
                count = len(confs)
                if count == 0:
                    sql = "INSERT INTO games(date,gameid,team_name_home,team_name_away,goals_home,goals_away) VALUES ('{}','{}','{}','{}','{}','{}')".format(ddate,gameId,team_name_home,team_name_away,goals_home,goals_away)
                    cursor.execute(sql)
                    connection.commit()
                else:
                    sql = "SELECT * FROM games WHERE gameid = %s"
                    cursor.execute(sql, (gameId,))
                    confs = cursor.fetchall()
                    for ff in confs:
                        if str(ff[2]) != gameId :
                            sql = "INSERT INTO game_away(date,gameid,fullname_player,toi) VALUES ('{}','{}','{}','{}')".format(ddate,gameId,ii[1],ii[3])
                            cursor.execute(sql)
                            connection.commit()
            i = i + 1
        start_date += delta
