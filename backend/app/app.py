import requests
import mysql.connector as mariadb
from flask import Flask, jsonify, render_template, request
import json
from database import insert_data
import time
import atexit
# from apscheduler.schedulers.background import BackgroundScheduler
import os
from dotenv import load_dotenv
load_dotenv()
db_user = os.getenv('db_user')
db_host = os.getenv('db_host')
db_password = os.getenv('db_password')


# from api import service_api

connection = mariadb.connect(
        host=db_host,
        ssl_ca='root.crt',
        port= 3306,
        user=db_user,
        password=db_password,
        database='backend')
cursor = connection.cursor()

# Function for additing information in mysql
insert_data()
    
# def job():
#     insert_data()

# scheduler = BackgroundScheduler()
# scheduler.add_job(func=job, trigger="interval", hours=24)
# scheduler.start()

#Function API for Frontend
# service_api()
app = Flask(__name__)

@app.route("/gameid/<int:id>", methods = ['GET'])
def gameid(id):
#Function for select data in tables game_home
    try:
        sql_game_home = "SELECT * FROM game_home WHERE gameid = %s"
        cursor.execute(sql_game_home, (id,))
        game_data_home= cursor.fetchall()
        fields_list_game_home = cursor.description
        column_list_game_home = []
        for i in fields_list_game_home:
            column_list_game_home.append(i[0])
        jsonData_list_game_home = []
        for row in game_data_home:
            data_dict_game_home = {}
            data_dict_game_home[column_list_game_home[4]] = row[4]
            data_dict_game_home[column_list_game_home[3]] = row[3]
            jsonData_list_game_home.append(data_dict_game_home)
    except:
        "Error Insert game_home"
#Function for select data in tables game_away
    try:
        sql_game_away = "SELECT * FROM game_away WHERE gameid = %s"
        cursor.execute(sql_game_away, (id,))
        game_data_away= cursor.fetchall()
        fields_list_game_away = cursor.description
        column_list_game_away = []
        for i in fields_list_game_away:
            column_list_game_away.append(i[0])
        jsonData_list_game_away = []
        for row in game_data_away:
            data_dict_game_away = {}
            data_dict_game_away[column_list_game_away[4]] = row[4]
            data_dict_game_away[column_list_game_away[3]] = row[3]
            jsonData_list_game_away.append(data_dict_game_away)
    except:
        "Error Select game_away"

#Function for select data in tables games
    try:
        sql = "SELECT * FROM games WHERE gameid = %s"
        cursor.execute(sql, (id,))
        games_data = cursor.fetchall()
        fields_list_games = cursor.description
        column_list_games = []
        for i in fields_list_games:
            column_list_games.append(i[0])   
        for row2 in games_data:
            result = {
            "Date": row[1],
            "GamesID": row[2],
                "Command": {
                    "Home": {
                        "Name" : row2[3],
                        "Goals": row2[5],
                        "Data": jsonData_list_game_home
                    },
                    "Away": {
                    "Name" : row2[4],
                    "Goals": row2[6],
                    "Data": jsonData_list_game_away
                    }
                }
        }
        return jsonify(result)
    except:
        "Error Select Games"

@app.route("/top3/<int:id>", methods = ['GET'])
def top3(id):
    try:
#Function for select data in tables top3_player
        ga = "SELECT * FROM game_away WHERE gameid = %s"
        gh = "SELECT * FROM game_home WHERE gameid = %s"
        cursor.execute(ga, (id,))
        top3_player_data= cursor.fetchall()
        cursor.execute(gh, (id,))
        top3_player_data2= cursor.fetchall()
        players_details = [*top3_player_data, *top3_player_data2]
        sorteed = sorted(players_details, reverse=True, key=lambda d: tuple(map(int, d[4].split(':'))))[:3]
        jsonData_list_top3_player = []
        for i in sorteed:
            data_dict_top3_player = {}
            data_dict_top3_player["fullname_player"] = i[3]
            data_dict_top3_player["toi"] = i[4]
            jsonData_list_top3_player.append(data_dict_top3_player)
            result = {
            "Date": i[1],
            "GamesID": i[2],
                "top3_player": {
                        "Data": jsonData_list_top3_player
                    }
            }
        return jsonify(result)
    except:
        "Error Select TOP3"
@app.route("/games", methods = ['GET'])
def games():
    try:
        cursor.execute("SELECT * FROM games")
        games_date = cursor.fetchall()
        fields_list = cursor.description
        column_list = []
        for i in fields_list:
            column_list.append(i[0])
        jsonData_list = []
        for row in games_date:
            data_dict = {}
            for i in range(len(column_list)):
                data_dict[column_list[i]] = row[i]
            jsonData_list.append(data_dict)
        return jsonify(jsonData_list)
    except:
        "Error Select Games"

if __name__ == "__main__":
    app.run(debug = True,host='0.0.0.0')
